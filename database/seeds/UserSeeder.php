<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'nur',
        	'email' => 'laylitanurh@gmail.com'
        ]);

        DB::table('users')->insert([
        	'name' => 'laylita',
        	'email' => 'laylitanurhilllalia@gmail.com'
        ]);
    }
}
