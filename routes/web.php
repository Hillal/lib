<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

//genre
Route::post('/genre/store', 'GenreController@store')->name('genre.store');
Route::get('/genre', 'GenreController@index');

//book
Route::post('/book/store', 'BookController@store')->name('book.store');
Route::get('/book', 'BookController@index');

//borrow
Route::post('/borrow/store', 'BorrowController@store')->name('borrow.store');
Route::get('/borrow', 'BorrowController@index');


