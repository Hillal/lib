@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Genre</h1>
	</section>


	<section class="content">
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Genre</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
                <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form action="/borrow/store" method="post" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                  </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label for="exampleInputEmail1">User</label>
                  <select class="form-control" name="user_id" required>
                    <option>-- Pilih Salah Satu --</option>
                    @foreach($User as $user)
                      <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Book</label>
                  <select class="form-control" name="book_id" required>
                    <option>-- Pilih Salah Satu --</option>
                    @foreach($Book as $book)
                      <option value="{{ $book->id }}">{{ $book->title }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label>Date</label>
                  <input type="date" class="form-control" name="date" placeholder="Isikan publisher ...">
                </div>
                <div class="form-group">
                  <label>Return</label>
                  <input type="date" class="form-control" name="return" placeholder="Isikan publisher ...">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
	</section>
</div>

@include('base.footer')