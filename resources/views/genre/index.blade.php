@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Genre</h1>
	</section>


	<section class="content">
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Genre</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
                <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form role="form" action="{{ route('genre.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                  </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Genre</label>
                  <input type="text" class="form-control" name="name" placeholder="Isikan genre ...">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Genre Table</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form action="/genre" method="GET">
                        </form>
              <table id="datatable" class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                </tr>

                @foreach($Genre as $item)
                  <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                  </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
	</section>
</div>

@include('base.footer')