<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Genre;
use Illuminate\Support\Facades\Session;

class GenreController extends Controller
{
    public function index()
    {
        $Genre = Genre::all();


        return view('genre.index',compact('Genre'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required'
    	]);

    	try {
    		$Genre = new Genre();
    		$Genre->name = $request->name;
    		$Genre->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back();
    	}
    }
}
