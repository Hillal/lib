<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Support\Facades\Session;

class BookController extends Controller
{

	 public function index()
    {
       //search
      $Book = Book::all();

        $Genre = Genre::select(['id', 'name'])->orderBy('name')->get();
     return view('book.index',compact('Book', 'Genre'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'title' => 'required',
    		'writer' => 'required',
    		'publisher' => 'required',
    		'genre_id' => 'required',
    	]);

    	try {
            Genre::where('id', $request->genre_id);

    		$Genre = Genre::all();
    		$Book = new Book();
    		$Book->title = $request->title;
    		$Book->writer = $request->writer;
    		$Book->publisher = $request->publisher;
    		$Book->genre_id = $request->genre_id;
    		$Book->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back();
    	}
    }
}
