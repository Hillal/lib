<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Borrow;
use App\User;
use App\Models\Book;
use Illuminate\Support\Facades\Session;


class BorrowController extends Controller
{
    public function index()
    {
      $Borrow = Borrow::all();
      $User = User::all();
      $Book = Book::all();

     return view('borrow.index',compact('User', 'Book', 'Borrow'));
    }

   public function store(Request $request)
   {
   		$this->validate($request,[
    		'user_id' => 'required',
    		'book_id' => 'required',
    		'date' => 'required',
    		'return' => 'required',
    	]);

    	try {
    		$Borrow = new Borrow();
    		$Borrow->user_id = $request->user_id;
    		$Borrow->book_id = $request->book_id;
    		$Borrow->date = $request->date;
    		$Borrow->return = $request->return;
    		$Borrow->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back();
    	}
   }
}
