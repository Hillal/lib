<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Visitor = User::all();

            $response=$Visitor;
            $code=200;
        } catch (Exception $e) {
            $code=500;
            $response=$e->getMessage();

        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
        ]);

        try{
            $Visitor = new User();
            $Visitor->name = $request->name;
            $Visitor->email = $request->email;

            $Visitor->save();

            $code = 200;
            $response = $Visitor;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 404;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try {
            $Visitor = User::findOrFail($id);

            $code = 200;
            $response = $Visitor;
        } catch (\Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
        ]);

        try {
            $Visitor = User::find($id);

            $Visitor->name = $request->name;
            $Visitor->email = $request->email;
            $Visitor->save();
            $code = 200;
            $response = $Visitor;
        } catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = 'data tidak ada';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Visitor = User::find($id);
            $Visitor->delete();

            $code = 200;
            $response = $Visitor;
        } catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}
