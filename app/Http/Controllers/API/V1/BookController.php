<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Models\Genre;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $Book = Book::all();
            $Response = $Book;
            $code = 200;
        }catch(Exception $e){
            $code = 500;
            $Response = $e->getMessage();
        }
        return apiResponseBuilder($code,$response
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'title' => 'required',
            'writer' =>'required',
            'publisher' => 'required',
            'genre_id' => 'required'
        ]);

        try{
            $Book = new Book();

            $Book->title = $request->title;
            $Book->writer = $request->writer;
            $Book->publisher = $request->publisher;
            $Book->genre_id = $request->genre_id;

            $Book->save();

            $code = 200;
            $response = $Book;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 404;
                $response = 'Isi data terlebih dahulu';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Book = Book::findOrFail($id);

            $code = 200;
            $response = $Book;
        } catch (\Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
               $this->validate($request,[
            'title' => 'required',
            'writer' => 'required',
            'publisher' => 'required',
            'genre_id' => 'required'
        ]);

        try {
            $Book = Book::find($id);

            $Book->title = $request->title;
            $Book->writer = $request->writer;
            $Book->publisher = $request->publisher;
            $Book->genre_id = $request->genre_id;

            $Book->save();
            $code = 200;
            $response = $Book;
        } catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = 'data tidak ada';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try {
            $Book = Book::find($id);
            $Book->delete();

            $code = 200;
            $response = $Book;
        } catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}