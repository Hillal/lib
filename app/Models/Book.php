<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'book';

    public function Genre()
    {
    	return $this->hasOne(Genre::class, "id", "genre_id");
    }
}
